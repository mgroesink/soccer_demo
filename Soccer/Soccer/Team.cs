﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soccer
{
    /// <summary>
    /// A class for soccer teams
    /// </summary>
    public class Team
    {
        #region Fields
        /// <summary>
        /// The name
        /// </summary>
        private string name;
        /// <summary>
        /// The city
        /// </summary>
        private string city;
        /// <summary>
        /// The stadium
        /// </summary>
        private string stadium;
        /// <summary>
        /// The year
        /// </summary>
        private int year;
        /// <summary>
        /// The coach
        /// </summary>
        private string coach;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        public string City
        {
            get { return city; }
            set { city = value.ToUpper(); }
        }

        /// <summary>
        /// Gets the age.
        /// </summary>
        /// <value>
        /// The age.
        /// </value>
        public int Age
        {
            get { return DateTime.Now.Year - year; }
        }
        #endregion

        #region Constructors
        // Blanco constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="Team"/> class.
        /// </summary>
        public Team()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Team"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="city">The city.</param>
        public Team(string name, string city)
        {
            this.name = name;
            this.city = city;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Team"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="city">The city.</param>
        /// <param name="stadium">The stadium.</param>
        /// <param name="year">The year.</param>
        /// <param name="coach">The coach.</param>
        public Team(string name, string city, string stadium, int year, string coach)
        {
            this.name = name;
            this.city = city;
            this.stadium = stadium;
            this.year = year;
            this.coach = coach;
        }

        #endregion
    }
}
