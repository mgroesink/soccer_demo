﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soccer
{
    class Program
    {
        private const char LINECHAR = '-';
        private const int LINELENGTH = 75;

        static void Main(string[] args)
        {

            // Create some teams
            Team teamA = new Team();
            Team teamB = new Team("FC Twente", "Enschede");
            Team teamC = new Team("FC Barcelona",
                "Barcelona", "Camp Nou", 1899, "Ernesto Valverde");
            teamA.Name = "Ajax";
            teamA.City = "Amsterdam";

            // Show the team info
            Console.WriteLine("Teams:");
            Console.WriteLine("".PadRight(LINELENGTH, LINECHAR));
            Console.WriteLine(
                "De naam van team A is {0} en dit team is {1} jaar oud.",
                teamA.Name, teamA.Age);
            Console.WriteLine(
                "De naam van team B is {0} en dit team is {1} jaar oud.",
                teamB.Name, teamB.Age);
            Console.WriteLine(
                "De naam van team C is {0} en dit team is {1} jaar oud.",
                teamC.Name, teamC.Age);

            // Create some players
            Player playerA = new Player("Frenkie", "de Jong");
            Player playerB = new Player("Lionel", "Messi",
                new DateTime(1987, 6, 24));
            Player playerC = new Player("Peet", "Bijen",
                new DateTime(1995, 1, 28), teamB, 400000d);
            Player playerD = new Player();
            Player playerE = new Player("Joey", "Drommel", new DateTime(1995, 2, 12, 20, 0, 0), teamB, 600000);

            // Show the names of the players
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Players:");
            Console.WriteLine("".PadRight(LINELENGTH, LINECHAR));
            Console.WriteLine("Name: " + playerA.GetFullName());
            Console.WriteLine("Name: " + playerB.GetFullName());
            Console.WriteLine("Name: " + playerC.GetFullName());
            Console.WriteLine("Name: " + playerD.GetFullName());
            Console.WriteLine("Name: " + playerE.GetFullName());

            // Create some arrays
            int[] numbers = new int[100]; // An array for 100 integers
            string[] names = new string[10]; // An array for 10 strings
            DateTime[] dates = new DateTime[25]; // An array voor 25 dates

            // Create some arrays with teams
            // Remember that you MUST fill in the maximum length end the type
            Team[] eredivisie = new Team[18]; // array for 18 teams
            Team[] eerstedivisie = new Team[20]; // array for 20 teams
            Team[] primeraDivison = new Team[20]; // array for 20 teams

            // Put teams in the arrys
            eredivisie[1] = teamA;
            eerstedivisie[0] = teamB;
            primeraDivison[0] = teamC;
            eredivisie[0] = new Team("PSV", "Eindhoven");

            // Delete teams from arrays 
            eredivisie[1] = null;
            primeraDivison[0] = null;

            // Get some teams from the arrays
            Team teamD = eredivisie[0];
            Team barca = primeraDivison[0];
            Team real = primeraDivison[4];

            // Create an ArrayList
            // Any type of data can be stored in the ArrayList
            ArrayList zooi = new ArrayList();
            zooi.Add(14);
            zooi.Add("Jaap");
            zooi.Add(teamC);
            zooi.Add(playerA);
            zooi.RemoveAt(3); // Remove the player
            zooi.Add("Pietje");

            // Get some values from the ArrayList
            // Don't forget to cast to the correct type
            int leeftijd = (int)zooi[0];
            Team team = (Team)zooi[2];

            // Create a list of teams
            // The list MUST be of a specified type but you don't have 
            // to assign a maximum length to the list
            List<Team> premierLeague = new List<Team>();
            premierLeague.Add(new Team("Manchester United", "Manchester"));
            premierLeague.Add(new Team("Manchester City", "Manchester"));
            premierLeague.Add(new Team("Liverpool", "Liverpool", "Anfield", 1892, "Jurgen Klopp"));
            premierLeague.Add(new Team("Arsenal", "Londen"));
            premierLeague.RemoveAt(3); // Verwijder Arsenal

            // Show info of all teams in the premier league
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Some teams in the Premier League");
            Console.WriteLine("".PadRight(LINELENGTH, LINECHAR));
            foreach (var item in premierLeague)
            {
                Console.WriteLine(
                "De naam van het team is {0} en dit team is {1} jaar oud.",
                item.Name, item.Age);
            }

            // Create some leagues
            League ere = new League();
            League keuken = new League()
            { Name = "Keuken Kampioen Divisie", Country = "Nederland" };
            ere.Name = "Eredivisie";
            ere.Country = "Nederland";

            // Add some teams to the leagues
            keuken.AddTeam(teamB); // Add FC twente
            ere.AddTeam(teamA); // Add Ajax

            // End of season 2018-2019
            // FC Twente promotes to league Eredivisie
            ere.AddTeam(teamB);
            keuken.RemoveTeam(teamB);

            Console.ReadKey();
        }
    }
}
