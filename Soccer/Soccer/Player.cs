﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soccer
{
    /// <summary>
    /// A Player class
    /// </summary>
    public class Player
    {

        #region Fields and Properties
        /// <summary>
        /// The first name
        /// </summary>
        private string firstName;

        /// <summary>
        /// The last name
        /// </summary>
        private string lastName;

        /// <summary>
        /// The birth date
        /// </summary>
        private DateTime birthDate;

        /// <summary>
        /// The team
        /// </summary>
        private Team team;

        /// <summary>
        /// The market value
        /// </summary>
        private double value;

        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="firstName">The first name.</param>
        /// <param name="lastName">The last name.</param>
        public Player(string firstName, string lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="firstName">The first name.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="birthDate">The birth date.</param>
        public Player(string firstName, string lastName,
            DateTime birthDate)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.birthDate = birthDate;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="firstName">The first name.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="birthDate">The birth date.</param>
        /// <param name="team">The team.</param>
        /// <param name="value">The value.</param>
        public Player(string firstName, string lastName,
            DateTime birthDate, Team team, double value)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.birthDate = birthDate;
            this.team = team;
            this.value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        public Player()
        {

        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the age.
        /// </summary>
        /// <returns></returns>
        public int GetAge()
        {
            int age = DateTime.Now.Year - birthDate.Year;

            // Check if player had his birthday this year
            // and subtract 1 from age if not
            if (DateTime.Now.Month < birthDate.Month ||
                (DateTime.Now.Month == birthDate.Month &&
                DateTime.Now.Day < birthDate.Day))
                age--;
            return age;
        }
        /// <summary>
        /// Gets the full name.
        /// </summary>
        /// <returns></returns>
        public string GetFullName()
        {
            return (firstName == null && lastName == null) ? "Unknown" :firstName + " " + lastName;
        }
        /// <summary>
        /// Transfers the player to a new team.
        /// </summary>
        /// <param name="newTeam">The new team.</param>
        public void Transfer(Team newTeam)
        {
            this.team = newTeam;
        } 
        #endregion

    }
}
