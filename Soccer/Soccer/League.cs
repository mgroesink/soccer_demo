﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soccer
{
    public class League
    {

        #region Fields and Properties
        // Auto implemented properties
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country { get; set; }
        /// <summary>
        /// The teams
        /// </summary>
        private List<Team> teams = new List<Team>();
        #endregion

        #region Methods
        /// <summary>
        /// Adds a new team.
        /// </summary>
        /// <param name="newTeam">The new team to add.</param>
        public void AddTeam(Team newTeam)
        {
            teams.Add(newTeam);
        }
        /// <summary>
        /// Removes a team.
        /// </summary>
        /// <param name="team">The team to remove.</param>
        public void RemoveTeam(Team team)
        {
            teams.Remove(team);
        }

        #endregion

    }
}
