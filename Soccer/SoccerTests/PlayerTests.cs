﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soccer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soccer.Tests
{
    [TestClass()]
    public class PlayerTests
    {
        [TestMethod()]
        public void GetAgeTest()
        {
            Team t = new Team("A" , "B" , "" , 1995,"");
            int age = DateTime.Now.Year - 1995;


            Assert.AreEqual(age, t.Age);
        }

        [TestMethod()]
        public void FullNameTest()
        {
            Player p = new Player();
            Assert.AreEqual("Unknown", p.GetFullName());
        }
    }
}